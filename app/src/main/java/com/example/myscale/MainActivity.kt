package com.example.myscale

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    private var res: Resources? = null
    private var configuration: Configuration? = null
    private var metrics: DisplayMetrics? = null
    private var sPref: SharedPreferences? = null

    companion object {

        // FIXME - тут непонятно - откуда брать программно этот параметр, ведь пользователь может вручную его менять,
        //         и при попытке его узнать будет получено значение из настроек системы, которые пользователь мог изменить,
        //         а параметры всех устройств знать невозможно
        private const val DEFAULT_DEVICE_DENSITY = 480f
        private var start = 0
        private var scaleRatio = 1.0f
    }

    override fun onResume() {
        super.onResume()

        if (start < 2){
            start++
            setScale()
        } else {
            start = 0
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        res = resources
        configuration = resources.configuration
        metrics = resources.displayMetrics
        sPref = getSharedPreferences("todolist", Context.MODE_PRIVATE)

        scaleRatio = loadScaleRatio()?: 1.0f

        tvCurrentScaleRatio.text = "Current Scale: $scaleRatio"
        seekBar.setOnSeekBarChangeListener(this)
        seekBar.progress = when(scaleRatio){
            0.75f -> 0
            1.0f -> 1
            1.5f -> 2
            else -> 1
        }
    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
        //TODO("Not yet implemented")
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
        //TODO("Not yet implemented")
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
        when  (p0?.progress){
            0 -> scaleRatio = 0.75f
            1 -> scaleRatio = 1.0f
            2 -> scaleRatio = 1.5f
        }
        saveScaleRatio(scaleRatio)
        setScale()
    }

    private fun setScale() {
        configuration = resources.configuration
        configuration?.densityDpi = (DEFAULT_DEVICE_DENSITY * scaleRatio).toInt()
        configuration?.fontScale = scaleRatio
        resources.updateConfiguration(configuration, resources.displayMetrics)
        recreate()
    }

    private fun saveScaleRatio(value: Float) {
        sPref?.edit()?.putFloat("scale_ratio", value)?.apply()
    }

    private fun loadScaleRatio(): Float? {
        return sPref?.getFloat("scale_ratio", 1.0f)
    }
}